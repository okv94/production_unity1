using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class youWinBanner : MonoBehaviour
{
    public Renderer rend;
    private void Start()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
    }
}
