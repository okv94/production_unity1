using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randomPush : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

	void OnCollisionEnter(Collision collission)
	{
		int fuerzaEnX = Random.Range(50, 100);
		int fuerzaEnY = Random.Range(100, 500);

		GetComponent<Rigidbody>().AddForce(fuerzaEnX, fuerzaEnY, 0);
		GetComponent<AudioSource>().pitch = Random.Range(0.5f, 1.5f);
        GetComponent<AudioSource>().volume = Random.Range(0.7f, 0.9f);
        GetComponent<AudioSource>().Play();
	}
}
