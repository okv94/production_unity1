using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class itemdestroy : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Se est� ejecutando");
        GetComponent<AudioSource>().pitch = Random.Range(0.5f, 1.0f);
        GetComponent<AudioSource>().volume = Random.Range(0.9f, 1.0f);
        GetComponent<AudioSource>().Play();
        GetComponent<MeshRenderer>().enabled = false;
        
    }

}
